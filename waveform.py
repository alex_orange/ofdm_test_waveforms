import numpy

def delay_rotate(waveform, samples):
    waveform_f = numpy.fft.fft(waveform)
    waveform_f *= numpy.exp(numpy.fft.fftshift(
        numpy.linspace(1j*numpy.pi, -1j*numpy.pi,
                       len(waveform), endpoint=False)*samples))
    return numpy.fft.ifft(waveform_f)

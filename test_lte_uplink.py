import waveform
import lte_uplink

a = lte_uplink.LTEUplink(64, 3)

print(a.waveform())
print(a.waveform())
print(max(abs(a.waveform())))

waveform_ = a.waveform()
print(a.calculate_evm(waveform_))
print(max(a.calculate_evm(waveform_)))

waveform_ = waveform.delay_rotate(a.waveform(), 0.01)
print(a.calculate_evm(waveform_))
print(max(a.calculate_evm(waveform_)))

waveform_ = waveform.delay_rotate(a.waveform(), 31)
print(a.calculate_evm(waveform_))
print(max(a.calculate_evm(waveform_)))

waveform_ = (1+0.1j)*waveform.delay_rotate(a.waveform(), 31)
print(a.calculate_evm(waveform_))
print(max(a.calculate_evm(waveform_)))


import numpy
import random

random.seed(1)

last_phase_offset = random.gauss(0, 1)
amplitude_offset = random.gauss(0, 1)

waveform_f = numpy.fft.fft(waveform_)
for i, _ in enumerate(waveform_f):
    last_phase_offset += 0.02*random.gauss(0, 1)
    amplitude_offset += 1.0*random.gauss(0, 1)
    amplitude_offset *= 0.9
    waveform_f[i-len(waveform_)//2] *= \
            numpy.exp(0.1*amplitude_offset+1j*last_phase_offset)

waveform_ = numpy.fft.ifft(waveform_f)

print(a.calculate_evm(waveform_))
print(max(a.calculate_evm(waveform_)))
b = a.evm_statistics(waveform_)
print(b.mean)
print(b.stdev)

print(a.occupied_carriers)
print(a.pilots)
print(a.data_carriers)

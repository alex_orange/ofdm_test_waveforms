import numpy
import test_waveform
import cmath
import qam
import collections.abc
import random
import statistics

# Until 3.10 can be expected
def linear_regression(x, y):
    mean_x = statistics.mean(x)
    mean_y = statistics.mean(y)

    beta = (sum([(x_-mean_x)*(y_-mean_y) for x_, y_ in zip(x, y)]) /
            sum([(x_-mean_x)*(x_-mean_x) for x_ in x]))

    alpha = mean_y - beta * mean_x

    return alpha, beta

class LTEUplink(test_waveform.TestWaveform):
    #            0   1         2  3   4         5   6          7    8
    pilot_prb = [1j, 0.6+0.8j, 1, -1, 0.8-0.6j, 1j, -0.6+0.8j, -1j, -1,
    #            9         10 11
                 0.6+0.8j, 1, 0.6+0.8j]
    pilot_subcarrier = 6
    pilot = 1j
    pilot_multiplier = 1j

    """
    Frequency plan:
        Bottom fill with 0's
        Middle physical_resource_blocks*12 carriers are filled. As per LTE, the
            frequency use is symmetric and is pushed by half a subcarrier to
            achieve this. This class will generate with the 0 or 6th subcarrier
            in the middle PRB being at DC and then will repeat the block in the
            time domain and shift the entire 2 blocks up by half the subcarrier
            offset.
        Top fill with 0's
    Every 6th subcarrier within a PRB will be treated as a pilot. This is
        different from LTE where pilots are not sent every block, but for now
        we shall only send one OFDM symbol. The middle rounded up PRB is used
        entirely for pilots (e.g. index 5 for a 10 or 11 PRB carrier).
    """
    def __init__(self, fft_size, physical_resource_blocks,
                 constellation=qam.QAM16(), random_seed=1):
        """
        fft_size - Size of the fft. For instance commonly 1024 for a 50 PRB
            carrier.
        physical_resource_blocks - The number of physical resource blocks
            (PRBs). 50 for a 10 MHz (actually 9 MHz with guard bands) carrier.
        qam - Which constellation to choose from. This can either be a
            constellation class in which case the same constellation will be
            used for all PRBs, or a sequence of constellation classes of length
            PRBs-1 which will be used for each PRB (except the pilot PRB).
        random_seed - The random seed to use for the random number generator.
        """
        # > because of the half subcarrier offset
        assert fft_size > 12*physical_resource_blocks
        assert fft_size % 2 == 0 # Some assumptions made for fft_shift

        self.fft_size = fft_size
        self.physical_resource_blocks = physical_resource_blocks

        if not isinstance(constellation, collections.abc.Sequence):
            constellation = [constellation]*(physical_resource_blocks-1)
        self.constellations = constellation

        self.half_carrier_offset = numpy.linspace(0, 2j*numpy.pi, 2*fft_size,
                                                  endpoint=False)
        self.half_carrier_offset = numpy.exp(self.half_carrier_offset)
        self.half_carrier_offset_down = self.half_carrier_offset.conjugate()

        self.random = random.Random(random_seed)

        self._waveform = None
        self._waveform_f = None

        self.occupied_carriers = [
            i >= self.first_subcarrier%fft_size or i <= self.last_subcarrier
            for i in range(fft_size)]
        self.pilots = [
            ((i+fft_size//2)%fft_size-fft_size//2) % 12 == \
                ((physical_resource_blocks + 1) % 2) * self.pilot_subcarrier or
            ((i >= fft_size-6 or i < 6) if
             physical_resource_blocks % 2 == 1 else
             (i < 12))
            for i in range(fft_size)]
        self.data_carriers = [
            a and not b
            for a, b in zip(self.occupied_carriers, self.pilots)]

    @property
    def first_subcarrier(self):
        return -6*self.physical_resource_blocks # 6 = 12 // 2

    @property
    def last_subcarrier(self):
        return 6*self.physical_resource_blocks-1 # 6 = 12 // 2

    def waveform_f(self):
        if self._waveform_f is not None:
            return self._waveform_f

        subcarriers = numpy.zeros(self.fft_size, numpy.complex64)

        # TODO: Use repeatable random
        pilot_prb = self.physical_resource_blocks // 2
        first_subcarrier = self.first_subcarrier

        pilot_subcarrier = self.pilot_subcarrier

        pilot = self.pilot
        pilot_mult = self.pilot_multiplier

        i = first_subcarrier
        prb = 0

        for constellation in self.constellations:
            if prb == pilot_prb:
                for j in range(12):
                    subcarriers[i] = self.pilot_prb[j]
                    i += 1
                prb += 1

            for j in range(12):
                if j == pilot_subcarrier:
                    subcarriers[i] = pilot
                    pilot *= pilot_mult
                else:
                    subcarriers[i] = constellation.random_symbol(self.random)
                i += 1

            prb += 1

        self._waveform_f = subcarriers

        return subcarriers

    def waveform(self):
        if self._waveform is not None:
            return self._waveform

        subcarriers = self.waveform_f()

        waveform = numpy.fft.ifft(subcarriers)
        waveform = numpy.tile(waveform, 2)
        waveform *= self.half_carrier_offset

        self._waveform = waveform

        return waveform

    def calculate_evm(self, waveform):
        fft_size = self.fft_size
        prbs = self.physical_resource_blocks

        waveform = numpy.array(waveform)
        # TODO: Decide what to do with the second half
        expected_waveform_f = self.waveform_f()

        waveform *= self.half_carrier_offset_down
        waveform_f = numpy.fft.fft(waveform[:fft_size])

        # Coarse correction
        pilot_prb_correction = [expected_waveform_f[i] / waveform_f[i]
                                for i in range(-6, 6)]
        pilot_prb_phases = numpy.unwrap(numpy.angle(pilot_prb_correction))
        
        intercept, slope = linear_regression(range(-6, 6), pilot_prb_phases)
        waveform_f *= numpy.exp(numpy.fft.fftshift(
            numpy.linspace(1j*(intercept-slope*(fft_size//2)),
                           1j*(intercept+slope*(fft_size//2)),
                           fft_size, endpoint=False)))

        first_subcarrier = self.first_subcarrier
        pilot = self.pilot_subcarrier

        # Fine correction
        first = first_subcarrier + pilot + fft_size//2
        last = first_subcarrier + pilot + fft_size//2 + prbs*12

        expected_pilots = numpy.fft.fftshift(expected_waveform_f)[
            first:last:12]
        waveform_pilots = numpy.fft.fftshift(waveform_f)[first:last:12]

        correction_factor = expected_pilots / waveform_pilots

        correction_factor_magnitude = abs(correction_factor)
        correction_factor_phase = numpy.unwrap(numpy.angle(correction_factor))

        interpolation_vector_b = numpy.linspace(0, 1, 12, endpoint=False)
        interpolation_vector_a = 1 - interpolation_vector_b
        correction_factor_magnitude_interpolated = \
                numpy.concatenate(
                    [numpy.zeros(first-6),
                     numpy.full(6, correction_factor_magnitude[0])]+
                    [interpolation_vector_a*a + interpolation_vector_b*b
                     for a, b in zip(correction_factor_magnitude[:-1],
                                     correction_factor_magnitude[1:])]+
                    [numpy.full(6, correction_factor_magnitude[-1]),
                     numpy.zeros(fft_size-last+6)])
        correction_factor_phase_interpolated = \
                numpy.concatenate(
                    [numpy.zeros(first-6),
                     numpy.full(6, correction_factor_phase[0])]+
                    [interpolation_vector_a*a + interpolation_vector_b*b
                     for a, b in zip(correction_factor_phase[:-1],
                                     correction_factor_phase[1:])]+
                    [numpy.full(6, correction_factor_phase[-1]),
                     numpy.zeros(fft_size-last+6)])
        correction_factor_interpolated = numpy.fft.fftshift(
                correction_factor_magnitude_interpolated * \
                numpy.exp(1j*correction_factor_phase_interpolated))

        waveform_f *= correction_factor_interpolated

        return [abs(a) for a, b in zip(waveform_f - expected_waveform_f,
                                       self.data_carriers)
                if b]

    def evm_statistics(self, waveform):
        return test_waveform.Statistics(self.calculate_evm(waveform))

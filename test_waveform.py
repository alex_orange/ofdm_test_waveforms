import functools
import statistics
import abc

class Statistics:
    def __init__(self, data):
        self.data = data

    @functools.cached_property
    def mean(self):
        return statistics.fmean(self.data)

    @functools.cached_property
    def stdev(self):
        return statistics.stdev(self.data)

    @functools.lru_cache(maxsize=10)
    def quantile(self, **kwargs):
        return statistics.quantile(self.data, **kwargs)

class TestWaveform(abc.ABC):
    @abc.abstractmethod
    def waveform(self):
        """
        Create the waveform to be transmitted (either by radio or signal
        generator).
        """

    @abc.abstractmethod
    def calculate_evm(self, waveform):
        """
        Calculate the array of EVM values and return it.
        """

    @abc.abstractmethod
    def evm_statistics(self, waveform):
        """
        Calculate the EVM and then from it the statistics of the EVM.

        Returns a Statistics object which can be used to get the mean, stdev,
            etc.
        """

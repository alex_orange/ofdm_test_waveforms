import itertools
import math

# Scaling factor for QAM constellations is 1/RMS of constellation points where
# "square" means square of the magnitude of the complex value.

class QAM16:
    constellation = [
        (a+1j*b)/math.sqrt(10)
        for a, b in itertools.product(range(-3,4,2), range(-3, 4, 2))]

    def random_symbol(self, random):
        """
        Effect on random:
            Calls random.choice on a 16 element array.
        """
        return random.choice(self.constellation)


class QAM64:
    constellation = [
        (a+1j*b)/math.sqrt(42)
        for a, b in itertools.product(range(-7,8,2), range(-7, 8, 2))]

    def random_symbol(self, random):
        """
        Effect on random:
            Calls random.choice on a 64 element array.
        """
        return random.choice(self.constellation)

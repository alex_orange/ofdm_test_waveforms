These are test waveforms that are OFDMish. Also included is code for putting
these on test devices such as a Vector Signal Generator and processing the
results back from a radio or Vector Signal Analyzer.

Uses for this code:
* Test receiver functionality against different TX gain settings.
* Test frontend for receive functionality.
* Test transmit EVM (with and without frontend).
